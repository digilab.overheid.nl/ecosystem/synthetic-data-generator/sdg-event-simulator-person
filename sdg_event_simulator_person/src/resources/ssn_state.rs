use bevy::prelude::*;

#[derive(Resource, Default)]
pub struct SsnState {
    count: u64,
}

impl SsnState {
    pub fn new_ssn(&mut self) -> u64 {
        let val = self.count;
        self.count += 1;
        val
    }

    pub fn get(&self) -> u64 {
        self.count
    }

    pub fn set(&mut self, count: u64) {
        self.count = count;
    }
}
