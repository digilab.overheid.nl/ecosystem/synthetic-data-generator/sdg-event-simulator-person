use std::collections::HashMap;

use bevy::prelude::*;
use uuid::Uuid;

#[derive(Resource, Default)]
pub struct SeedState {
    data: HashMap<Uuid, Entity>,
}

impl SeedState {
    pub fn push(&mut self, id: Uuid, index: Entity) {
        self.data.insert(id, index);
    }

    pub fn get(&self, id: &Uuid) -> Option<Entity> {
        self.data.get(id).map(|v| v.to_owned())
    }
}
