use bevy_turborand::{DelegatedRng, RngComponent};
use chrono::NaiveDateTime;
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use uuid::{Builder as UuidBuilder, Uuid};

pub use sdg_event_simulator_common::{config::SdgConfig, event::Event};

pub const BIRTH_TOPIC: &str = "events.frp.persoon.GeboorteVastgesteld";
pub const NAME_TOPIC: &str = "events.frp.persoon.NamenVastgesteld";
pub const DEATH_TOPIC: &str = "events.frp.persoon.OverlijdenVastgesteld";
pub const ADDRESS_TOPIC: &str = "events.frp.persoon.AdresIngeschreven";

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Municipality {
    code: String,
    name: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventBirthData {
    #[serde(rename = "nationalInsuranceNumber")]
    pub ssn: u64,
    #[serde(rename = "placeOfBirth")]
    pub place_of_birth: Municipality,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventNameData {
    #[serde(rename = "givenName")]
    pub given_name: String,
    #[serde(rename = "surName")]
    pub surname: String,
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct EventAddressRegistationData {
    pub address: String,
}

pub fn birth_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    subject_id: Uuid,
    ssn: u64,
) -> Value {
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

    let data = EventBirthData {
        ssn,
        place_of_birth: Municipality {
            code: "0996".into(),
            name: "Garderen".into(),
        },
    };

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![subject_id],
        event_type: "GeboorteVastgesteld".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}

pub fn name_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    subject_id: Uuid,
    given_name: &str,
    surname: &str,
) -> Value {
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

    let data = EventNameData {
        given_name: given_name.into(),
        surname: surname.into(),
    };

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![subject_id],
        event_type: "NamenVastgesteld".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}

pub fn death_event(rng: &mut RngComponent, now: &NaiveDateTime, subject_id: Uuid) -> Value {
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![subject_id],
        event_type: "OverlijdenVastgesteld".into(),
        event_data: json!({}),
    };

    serde_json::to_value(event).unwrap()
}

pub fn address_registration_event(
    rng: &mut RngComponent,
    now: &NaiveDateTime,
    subject_id: Uuid,
    address: Uuid,
) -> Value {
    let mut bytes = [0; 16];
    rng.fill_bytes(&mut bytes);
    let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

    let data = EventAddressRegistationData {
        address: address.to_string(),
    };

    let event = Event {
        id: stable_uuid,
        occured_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        registered_at: now.format(SdgConfig::TIMEFORMAT).to_string(),
        subject_ids: vec![subject_id],
        event_type: "AdresIngeschreven".into(),
        event_data: serde_json::to_value(data).unwrap(),
    };

    serde_json::to_value(event).unwrap()
}
