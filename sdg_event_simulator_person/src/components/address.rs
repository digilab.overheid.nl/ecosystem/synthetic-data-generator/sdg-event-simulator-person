use std::str::FromStr;

use bevy::prelude::*;
use bevy_turborand::{DelegatedRng, RngComponent};
use chrono::Duration;
use sdg_event_simulator_common::{
    bevy::resources::clock_state::ClockState, chance::PercentageChance,
};
use uuid::Uuid;

#[derive(Component, Clone, Debug)]
pub struct Address {
    pub id: Uuid,
}

impl Address {
    pub fn new(id: &str) -> Self {
        Self {
            id: Uuid::from_str(id).unwrap(),
        }
    }

    pub fn event_occurs(rng: &mut RngComponent, time: &ClockState) -> bool {
        let rand = rng.u64(0..u64::MAX);
        let chance = PercentageChance::new(10.0)
            .scale(Duration::hours(24), time.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }
}
