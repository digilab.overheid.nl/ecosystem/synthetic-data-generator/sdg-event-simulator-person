use bevy::prelude::*;
use bevy_turborand::prelude::*;
use chrono::NaiveDateTime;

mod address;
mod age;
mod id;
mod person;
mod person_name;
mod pregnant;

pub use address::Address;
pub use age::Age;
pub use id::Id;
pub use person::Person;
pub use person_name::PersonName;
pub use pregnant::{CanBePregnant, IsPregnant};

#[derive(Bundle)]
pub struct PersonBundle {
    pub person: Person,
    pub rng: RngComponent,
    pub age: Age,
    pub id: Id,
}

impl PersonBundle {
    pub fn new(global_rng: &mut ResMut<GlobalRng>, time: NaiveDateTime, id: u64) -> Self {
        let mut rng = RngComponent::from(global_rng);

        let age = Age::new(&mut rng, time);
        let id = Id::new(&mut rng, id);

        Self {
            person: Person,
            rng,
            age,
            id,
        }
    }
}
