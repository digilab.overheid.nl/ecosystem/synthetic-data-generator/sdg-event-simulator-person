use std::fmt::Display;

use bevy::prelude::*;
use bevy_turborand::{DelegatedRng, RngComponent};
use chrono::Duration;
use sdg_event_simulator_common::{
    bevy::resources::clock_state::ClockState, chance::PercentageChance,
};

#[derive(Component, Clone, Debug)]
pub struct PersonName {
    pub given_name: String,
    pub surname: String,
}

impl Display for PersonName {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{} {}", self.given_name, self.surname)
    }
}

impl PersonName {
    pub fn new(given_name: String, surname: String) -> Self {
        Self {
            given_name,
            surname,
        }
    }

    pub fn event_occurs(rng: &mut RngComponent, time: &ClockState) -> bool {
        let rand = rng.u64(0..u64::MAX);
        let chance = PercentageChance::new(25.0)
            .scale(Duration::hours(24), time.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }
}
