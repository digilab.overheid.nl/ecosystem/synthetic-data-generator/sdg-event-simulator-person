use bevy::prelude::*;
use bevy_turborand::prelude::*;
use chrono::{Duration, NaiveDateTime};
use sdg_event_simulator_common::{
    bevy::resources::clock_state::ClockState, chance::PercentageChance,
};
use statrs::distribution::{Continuous, Normal};

const DAYS_PER_YEAR: f64 = 365.0;

#[derive(Component)]
pub struct Age {
    pub birth_date: NaiveDateTime,
    pub lifespan: Duration,
}

impl Age {
    pub fn new(rng: &mut RngComponent, birth_date: NaiveDateTime) -> Self {
        let lifespan = Duration::days(rng.i64(0..(99 * 365)));
        Self {
            birth_date,
            lifespan,
        }
    }

    pub fn age(&self, now: NaiveDateTime) -> Duration {
        now.signed_duration_since(self.birth_date)
    }

    fn is_fertile(&self, now: NaiveDateTime) -> bool {
        let age = self.age(now);
        age > Duration::days(365 * 18) && age < Duration::days(65 * 365)
    }

    pub fn event_death_occurs(&self, now: NaiveDateTime) -> bool {
        let age = self.age(now);
        age > self.lifespan
    }

    pub fn event_pregnancy_occurs(&self, rng: &mut RngComponent, clock_state: &ClockState) -> bool {
        let now = clock_state.get_time();

        if !self.is_fertile(now) {
            return false;
        }
        let rand = rng.u64(0..u64::MAX);

        let chance = PercentageChance::new(daily_preg_chance(self.age(now)))
            .scale(Duration::hours(24), clock_state.time_per_tick)
            .fraction_of_u64();

        rand < chance
    }
}

fn daily_preg_chance(age: Duration) -> f64 {
    let age = age.num_days() as f64 / DAYS_PER_YEAR;
    let center = 35.0;
    let std_deviation = 10.0; // Adjust the standard deviation to control the spread of the distribution
    let scale = 1.0;

    let normal = Normal::new(center, std_deviation).unwrap();

    let value = normal.pdf(age) * scale;

    if value < 0.0 {
        0.0
    } else if value > scale {
        scale
    } else {
        value
    }
}
