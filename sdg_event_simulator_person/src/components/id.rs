use bevy::prelude::*;
use bevy_turborand::{DelegatedRng, RngComponent};
use uuid::{Builder as UuidBuilder, Uuid};

#[derive(Component)]
pub struct Id {
    pub uuid: Uuid,
    pub ssn: u64,
}
impl ToString for Id {
    fn to_string(&self) -> String {
        self.uuid.to_string()
    }
}
impl Id {
    pub fn new(rng: &mut RngComponent, id: u64) -> Self {
        let mut bytes = [0; 16];
        rng.fill_bytes(&mut bytes);
        let stable_uuid = UuidBuilder::from_bytes(bytes).into_uuid();

        Self {
            uuid: stable_uuid,
            ssn: id,
        }
    }
}
