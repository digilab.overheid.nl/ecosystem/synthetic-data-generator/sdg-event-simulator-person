mod address;
mod birth;
mod death;
mod init;
mod name;

pub mod init_systems {
    pub use super::init::inspect_seeded;
    pub use super::init::load_seed_addresses;
    pub use super::init::load_seed_births;
    pub use super::init::load_seed_deaths;
    pub use super::init::load_seed_names;
}

pub mod update_systems {
    pub use super::address::sim_address_registration;
    pub use super::birth::sim_birth;
    pub use super::birth::sim_pregnancy;
    pub use super::death::sim_death;
    pub use super::name::sim_naming;
}
