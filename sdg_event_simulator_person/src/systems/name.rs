use bevy::prelude::*;
use bevy_turborand::prelude::*;
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};

use crate::{
    components::*,
    data::names::NAMES,
    event::{name_event, NAME_TOPIC},
};

#[allow(clippy::type_complexity)]
pub fn sim_naming(
    mut commands: Commands,
    mut query: Query<
        (Entity, &mut RngComponent, &Id, &Parent),
        (With<Person>, Without<PersonName>),
    >,
    name_query: Query<&PersonName>,
    clock_state: Res<ClockState>,
    nats_state: Res<NatsState>,
) {
    for (entity, mut rng, id, parent) in query.iter_mut() {
        if !PersonName::event_occurs(&mut rng, &clock_state) {
            continue;
        }

        let parent_id = parent.get();

        let parent_name = name_query.get_component::<PersonName>(parent_id);
        let surname = parent_name
            .map(|p| p.surname.clone())
            .unwrap_or_else(|_| format!("O #{}", entity.index()));

        let name_i = rng.usize(0..NAMES.len());
        let given_name = NAMES[name_i].to_string();

        debug!(
            time = clock_state.get_time().to_string(),
            eid = entity.index(),
            given_name = given_name,
            surname = surname,
            "NAME    "
        );

        let entity_cmd = commands.get_entity(entity);

        if let Some(mut entity_cmd) = entity_cmd {
            entity_cmd.insert(PersonName::new(given_name.clone(), surname.clone()));

            let message = name_event(
                &mut rng,
                &clock_state.get_time(),
                id.uuid,
                &given_name,
                &surname,
            );

            nats_state
                .connection
                .publish(NAME_TOPIC, message.to_string())
                .unwrap();
        }
    }
}
