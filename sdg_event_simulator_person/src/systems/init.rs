use bevy::prelude::*;
use bevy_turborand::prelude::*;
use chrono::NaiveDateTime;
use nats::jetstream::{
    self, BatchOptions, ConsumerConfig, JetStream, JetStreamOptions, PullSubscribeOptions,
    PullSubscription,
};
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};
use tracing::warn;

use crate::{
    components::{Address, Age, CanBePregnant, Id, Person, PersonBundle, PersonName},
    event::{
        Event, EventAddressRegistationData, EventBirthData, EventNameData, ADDRESS_TOPIC,
        BIRTH_TOPIC, DEATH_TOPIC, NAME_TOPIC,
    },
    resources::{SeedState, SsnState},
};

#[derive(Component)]
pub struct FromSeed;

pub fn get_seed_stream(nats_uri: &str, topic: &str) -> PullSubscription {
    let connection = nats::connect(nats_uri).expect("Could not connect to nats");
    let options = JetStreamOptions::new();
    let stream = JetStream::new(connection, options);

    stream
        .pull_subscribe_with_options(
            topic,
            &PullSubscribeOptions::new().consumer_config(ConsumerConfig {
                // durable_name: Some("sdg_person".to_string()),
                deliver_policy: jetstream::DeliverPolicy::All,
                replay_policy: jetstream::ReplayPolicy::Instant,
                filter_subject: topic.to_string(),
                ..Default::default()
            }),
        )
        .unwrap()
}

pub fn load_seed_births(
    mut commands: Commands,
    mut global_rng: ResMut<GlobalRng>,
    nats_state: Res<NatsState>,
    mut seed_state: ResMut<SeedState>,
    mut ssn_state: ResMut<SsnState>,
) {
    info!("Load `seed data birth` start");

    let birth_stream = get_seed_stream(&nats_state.uri, BIRTH_TOPIC);

    loop {
        let messages = birth_stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;
        for message in messages {
            // Parse data
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let event_data: EventBirthData = serde_json::from_value(event.event_data).unwrap();
            let birth_date =
                NaiveDateTime::parse_from_str(&event.occured_at, "%Y-%m-%dT%H:%M:%S%.fZ").unwrap();
            let subject = event.subject_ids[0];

            // Create components
            let mut rng = RngComponent::from(&mut global_rng);
            let age = Age::new(&mut rng, birth_date);
            let ssn = event_data.ssn;
            let id = Id { uuid: subject, ssn };

            let mut person = PersonBundle {
                person: Person,
                rng,
                age,
                id,
            };

            // Create entitiy
            let entity = if person.rng.bool() {
                commands.spawn((person, CanBePregnant, FromSeed)).id()
            } else {
                commands.spawn((person, FromSeed)).id()
            };

            // Update state
            if ssn_state.get() < ssn {
                ssn_state.set(ssn);
            }
            seed_state.push(subject, entity);

            // Finish
            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data birth` end");
            break;
        }
    }
}

pub fn load_seed_names(
    mut commands: Commands,
    nats_state: Res<NatsState>,
    seed_state: Res<SeedState>,
) {
    info!("Load `seed data name` start");

    let birth_stream = get_seed_stream(&nats_state.uri, NAME_TOPIC);

    loop {
        let messages = birth_stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;
        for message in messages {
            // Parse data
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let event_data: EventNameData = serde_json::from_value(event.event_data).unwrap();
            let subject = event.subject_ids[0];

            // Find entitiy
            let entity = seed_state.get(&subject).map(|se| commands.get_entity(se));

            // Create and attach component
            if let Some(Some(mut entity_cmd)) = entity {
                let name = PersonName::new(event_data.given_name, event_data.surname);
                entity_cmd.insert(name);
            }

            // Finish
            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data name` end");
            break;
        }
    }
}

pub fn load_seed_addresses(
    mut commands: Commands,
    nats_state: Res<NatsState>,
    seed_state: Res<SeedState>,
) {
    info!("Load `seed data address` start");

    let birth_stream = get_seed_stream(&nats_state.uri, ADDRESS_TOPIC);

    loop {
        let messages = birth_stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;
        for message in messages {
            // Parse data
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let event_data: EventAddressRegistationData =
                serde_json::from_value(event.event_data).unwrap();
            let subject = event.subject_ids[0];

            // Find entitiy
            let entity = seed_state.get(&subject);

            // Create and attach component
            if let Some(entity) = entity {
                let name = Address::new(&event_data.address);
                commands.entity(entity).insert(name);
            }

            // Finish
            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data address` end");
            break;
        }
    }
}

pub fn load_seed_deaths(
    mut commands: Commands,
    nats_state: Res<NatsState>,
    seed_state: Res<SeedState>,
) {
    info!("Load `seed data death` start");

    let birth_stream = get_seed_stream(&nats_state.uri, DEATH_TOPIC);

    loop {
        let messages = birth_stream.fetch(BatchOptions {
            batch: 100,
            expires: None,
            no_wait: true,
        });

        let messages = match messages {
            Ok(m) => m,
            Err(e) => {
                error!("Error loading events batch {}", e);
                break;
            }
        };

        let mut message_count = 0;
        for message in messages {
            // Parse data
            let data = std::str::from_utf8(&message.data).unwrap();
            let event: Event = serde_json::from_str(data).unwrap();
            let subject = event.subject_ids[0];

            // Find entity
            let entity = seed_state.get(&subject).unwrap();

            // Remove entity
            commands.entity(entity).despawn();

            // Finish
            message.ack().unwrap();
            message_count += 1;
        }

        if message_count == 0 {
            info!("Load `seed data death` end");
            break;
        }
    }
}

pub fn inspect_seeded(
    mut query: Query<(&Id, &Age, Option<&PersonName>), With<FromSeed>>,
    clock_state: Res<ClockState>,
) {
    info!("inspect_seeded");

    let count = query.iter().count();
    for (id, age, name) in query.iter_mut() {
        match name {
            Some(name) => info!(
                "Found id: {}, name: {}, age: {}",
                id.uuid,
                name,
                age.age(clock_state.get_time()).num_days() / 365
            ),
            None => warn!("Found: {}", id.uuid),
        };
    }

    info!("inspect_seeded: total found {}", count);
}
