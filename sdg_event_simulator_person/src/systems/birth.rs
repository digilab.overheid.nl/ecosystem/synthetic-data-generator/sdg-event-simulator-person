use bevy::prelude::*;
use bevy_turborand::prelude::*;
use sdg_event_simulator_common::bevy::resources::{clock_state::ClockState, nats_state::NatsState};

use crate::{
    components::*,
    event::{birth_event, BIRTH_TOPIC},
    SsnState,
};

#[allow(clippy::type_complexity)]
pub fn sim_pregnancy(
    mut commands: Commands,
    mut query: Query<
        (&mut RngComponent, Entity, &PersonName, &Age),
        (With<Person>, With<CanBePregnant>, Without<IsPregnant>),
    >,
    clock_state: Res<ClockState>,
) {
    for (mut rng, entity, name, age) in query.iter_mut() {
        let now = clock_state.get_time();
        if age.event_pregnancy_occurs(&mut rng, &clock_state) {
            debug!(
                time = now.to_string(),
                f_id = entity.index(),
                name = name.surname,
                "PREGNANT"
            );

            let entity_cmd = commands.get_entity(entity);

            if let Some(mut entity_cmd) = entity_cmd {
                entity_cmd.insert(IsPregnant { start: now });
            }
        }
    }
}

#[allow(clippy::type_complexity)]
pub fn sim_birth(
    mut commands: Commands,
    mut global_rng: ResMut<GlobalRng>,
    mut query: Query<(Entity, &mut RngComponent, &PersonName, &IsPregnant), With<Person>>,
    clock_state: Res<ClockState>,
    nats_state: Res<NatsState>,
    mut ssn_state: ResMut<SsnState>,
) {
    for (entity, mut rng, name, preg) in query.iter_mut() {
        let now = clock_state.get_time();

        if preg.is_complete(now) {
            let child_ssn = ssn_state.new_ssn();
            let mut child_person = PersonBundle::new(&mut global_rng, now, child_ssn);
            let child_uuid = child_person.id.uuid;

            let child = if child_person.rng.bool() {
                commands.spawn((child_person, CanBePregnant)).id()
            } else {
                commands.spawn(child_person).id()
            };
            debug!(
                time = now.to_string(),
                parent_id = entity.index(),
                name = name.surname,
                cid = child.index(),
                "BIRTH   "
            );

            let message = birth_event(&mut rng, &now, child_uuid, child_ssn);

            nats_state
                .connection
                .publish(BIRTH_TOPIC, message.to_string())
                .unwrap();

            commands.entity(entity).push_children(&[child]);
            commands.entity(entity).remove::<IsPregnant>();
        }
    }
}
