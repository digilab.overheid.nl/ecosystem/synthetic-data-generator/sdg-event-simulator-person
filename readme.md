# SDG event simulator person
The simulators goal is to generate realistic events concerning `persons`.
External systems can then take these events, interpret them for their own applications to build a test set.
When multiple parties do this a coherent set of test data can be derived across the applications, improving the ability to test the independend connected systems.

Simulating the events is done by ingesting seed data to build a "world", based on this "world" the simulator will walk through time and generate events based on a deterministic random number generator (RNG), and statistical data/functions.

The events will be stored and retrieved from [NATS Jetstream](https://docs.nats.io/jetstream/).

## Core
The core of the system is built on the [entity component system (ECS)](https://en.wikipedia.org/wiki/Entity_component_system) from the [bevy game engine](https://bevyengine.org/learn/book/getting-started/ecs/)

The ECS provides the main loop of the simulator, in this loop `systems` query `enities` based on their `components`.

- `Entities`: the objects being simulated, in this case `Person`
- `Components`: properties of the objects being simulated, e.g. `Age`, `Name`, `IsPregnant`
- `Systems`: functions that query and change the components of an `entitiy`, e.g. `SimPregnancy`, `SimBirth`. Emits events when relevant

## Plugins and Resourcres
Plugins and Resources provide connectivity and state to `Systems`, e.g. `RngPlugin`, `NatsState`, `ClockState`

## Data
Static data that is used in the simulation, e.g. A list of common names.

## System state diagram
```mermaid
stateDiagram-v2
    [*] --> ApplicationSetup
    ApplicationSetup --> WorldSetup: Application configuration ready
    WorldSetup --> WaitForClock: Resources and Plugins ready
    WaitForClock  --> PreStartup: ClockTick(1)
    state PreStartup {
        [*] --> LoadSeedBirths
        LoadSeedBirths --> [*]
    }
    PreStartup --> StartUp
    state StartUp {
        [*] --> LoadSeedNames
        LoadSeedNames --> LoadSeedAddresses
        LoadSeedAddresses --> [*]
    }
    StartUp --> PostStartup
    state PostStartup {
        [*] --> LoadSeedDeaths
        LoadSeedDeaths --> [*]
    }
    PostStartup --> Update
    state Update {
        [*] --> SimPregnancy
        SimPregnancy --> SimBirth
        SimBirth --> SimNaming
        SimNaming --> SimAddressRegistation
        SimAddressRegistation --> SimDeath
        SimDeath --> [*]
    }
    Update --> WaitForClock
    WaitForClock --> Update: ClockTick(>1)
    WaitForClock --> WaitForEnd: RunFlag != RUN
    WaitForEnd --> [*]: RunFlag == QUIT
```
