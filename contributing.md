# What to run
- Nats: `make nats`
- Nats init: `make run_init_docker`
- Seeding: in project [sdg-realm-seeder](https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-realm-seeder) `cargo run -p seed_realm -- --nats-stream="nats://localhost:4222/events" --num-instances="1000"`
- Seed: `curl -X POST 127.0.0.1:8080/seed -H 'Content-Type: application/json' -d'{}'`
- Nats admin: `make nats_admin`, then in the container run `nats subscribe "events.>"` (or install nats-admin locally and run this command)
- Create the `time` and `event` streams, see commands below
- Simulator: `make run`
- Clock: `make run_time_docker`, or use [SDG Orchestrator](https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator)

# Nats

## Nats:
```sh
$ docker run --rm -it --name=nats --network=sdg -p=4222:4222 docker.io/library/nats --js
```

## Nats admin:
```sh
docker run --rm -it --network=sdg --entrypoint=/bin/sh docker.io/natsio/nats-box
nats context add nats --server nats --select
```

Init (automatically done by the init container of <https://gitlab.com/digilab.overheid.nl/ecosystem/synthetic-data-generator/sdg-orchestrator>):
```sh
nats stream add --subjects='events.>' --storage=memory --replicas=1 --retention=limits --discard=new --max-msgs=-1 --max-msgs-per-subject=-1 --max-bytes=128MB --max-age=1M --max-msg-size=1MB --dupe-window=1m --no-allow-rollup --deny-delete --no-deny-purge 'events'
nats stream add --subjects='time' --storage=memory --replicas=1 --retention=limits --discard=old --max-msgs=8 --max-msgs-per-subject=-1 --max-bytes=1KB --max-age=1M --max-msg-size=32 --dupe-window=1m --no-allow-rollup --no-deny-delete --no-deny-purge 'time'
```

purge:
```sh
nats stream purge events --force
```

subscribe:
```sh
nats subscribe  "events.frp.>"
```

reset consumer:
```sh
nats consumer rm sdg_person
```

# Events

```json
{
  "id": "642f9496-9fd7-48ae-9d84-da02a01a211d",
  "occurredAt": "1972-08-06T00:01:05.861778026Z",
  "registeredAt": "1972-08-21T00:26:05.861778026Z",
  "subjectIds": [
    "de0a30cf-82f6-4d96-a8c1-c0356d22090b"
  ],
  "eventType": "PerceelGeregistreerd",
  "eventData": {
    "address": {
      "location": "Staphorst",
      "number": 81,
      "postalCode": "8480FU",
      "streetName": "Disselkade"
    }
  }
},
{
  "id": "8e608cc7-7692-4d47-9941-6bd66f3eb593",
  "occurredAt": "1994-03-27T19:10:05.861918319Z",
  "registeredAt": "1994-04-21T15:30:05.861918319Z",
  "subjectIds": [
    "d08e5f15-b93f-404d-bda4-38bb9bbece55"
  ],
  "eventType": "VoertuigGeregistreerd",
  "eventData": {
    "model": "Suzuki SX4",
    "numberPlate": "XD-980-MA"
  }
},
{
  "id": "65c9996e-3c67-448a-8d51-e0e95823dcd4",
  "occurredAt": "2010-07-28T06:32:05.861778026Z",
  "registeredAt": "2010-08-18T06:36:05.861778026Z",
  "subjectIds": [
    "c202d24a-dcba-4b44-8b2c-5bffb70ae32a"
  ],
  "eventType": "GeboorteVastgesteld",
  "eventData": {
    "nationalInsuranceNumber": 14064123,
    "placeOfBirth": "Garderen"
  }
},
{
  "id": "f69cd0c2-83d7-4c4d-a04f-d9d5b29cef25",
  "occurredAt": "2010-08-18T06:36:05.861778026Z",
  "registeredAt": "2010-09-12T19:57:05.861778026Z",
  "subjectIds": [
    "c202d24a-dcba-4b44-8b2c-5bffb70ae32a"
  ],
  "eventType": "NamenVastgesteld",
  "eventData": {
    "givenName": "Lily",
    "surName": "Bakker"
  }
},
{
  "id": "f3a34a37-8e94-45b8-92f8-4739152b1645",
  "occurredAt": "2010-09-13T07:11:05.861778026Z",
  "registeredAt": "2010-09-25T05:09:05.861778026Z",
  "subjectIds": [
    "c202d24a-dcba-4b44-8b2c-5bffb70ae32a"
  ],
  "eventType": "AdresIngeschreven",
  "eventData": {
    "address": "de0a30cf-82f6-4d96-a8c1-c0356d22090b"
  }
}
```
